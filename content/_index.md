+++
title = "Braus"
description = "A small application to select a browser every time you click a link anywhere."
+++

A small application to select a browser every time you click a link anywhere. This is especially useful if you have multiple browsers or profiles, and you want to be able to open certain links in certain browsers. Very useful for web developers, but also for a lot of other people.

GNU/Linux alternative to apps such as Choosy/Browserchooser

A package is available for Archlinux [in the AUR](https://aur.archlinux.org/packages/braus-git/)

If you wish to package the app for your distro, please [open an issue](https://github.com/properlypurple/braus/issues/) and we'll work on it together.
![Screenshot of Braus app](/screenshot.png "Screenshot of Braus app")

### How to build

Note: You need gtk, python3, and gobject installed. In most cases, you shouldn't need to install anything if you're on a gtk environment.

```
git clone https://github.com/properlypurple/braus
cd braus
meson build --prefix=/usr
cd build
ninja
ninja install
```
